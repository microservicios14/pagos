package main

import (
	"io/ioutil"
	"encoding/json"
	"fmt"
	"html"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/", Index)
	router.HandleFunc("/records", GetPayments)
	log.Fatal(http.ListenAndServe(":8003", router))
}

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
}



func GetPayments(w http.ResponseWriter, r *http.Request) {
	// creamos un mapa donde almacenaremos los registros del archivo .json
	// creamos un mapa donde almacenaremos los registros del archivo .json
	pag := make(map[int]string)
    
	// leer el contenido del archivo de los pagos
	//se almacena en un arreglo de bytes
	contenido_Json, err := ioutil.ReadFile("/data/payment_records.json")
	//Si no se puedo leer manda un error
	if err != nil{
		w.WriteHeader(500)
		fmt.Fprintf(w, "%q Error al leer el archivo", html.EscapeString(r.URL.Path))
	}
	
	// se decodifican los datos del archivo payment_records.json
	// y se guardan en el mapa
	json.Unmarshal([]byte(contenido_Json),&pag)
	
	//escribimos un header que indique que el proceso se realizó correctamente
	w.WriteHeader(200)
	// mandamos el header y el contenido del archivo con los pagos
	fmt.Fprintf(w, string(contenido_Json))
}
